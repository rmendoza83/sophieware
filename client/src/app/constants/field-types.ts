export class FIELD_TYPES {
    public static STRING = 'string';
    public static TEXT = 'memo';
    public static BOOLEAN = 'boolean';
    public static INTEGER = 'integer';
    public static DECIMAL = 'decimal';
    public static MONEY = 'money';
    public static DATE = 'date';
    public static TIME = 'time';
    public static DATETIME = 'datetime';
    public static LOOKUP = 'lookup';
    public static LOOKUP_API = 'lookup_api';
}