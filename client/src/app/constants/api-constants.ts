export class API_CONSTANTS {
    public static LIST = 'getlist';
    public static GET = 'get';
    public static INSERT = 'insert';
    public static UPDATE = 'update';
    public static DELETE = 'delete';
    public static PATCH = 'patch';
    public static EMPTY = 'empty';
}