import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HTTPBaseService } from './http-base.service';
import { Observable } from 'rxjs';
import { IAPIResponse } from 'src/app/models/interfaces/api-response-interface';
import { APISecurityTokenModel } from 'src/app/models/api-security-token-model';

@Injectable({
  providedIn: 'root'
})
export class BaseFormService extends HTTPBaseService {

  constructor (
    private http: HttpClient) {
      super(http);
  }

  public setModuleName(moduleName: string) {
    this.moduleName = moduleName;
  }

  public setServiceName(serviceName: string) {
    this.serviceName = serviceName;
  }

  public setAPISecurityToken(APISecurityToken: APISecurityTokenModel) {
    this.APISecurityToken = APISecurityToken;
  }

  public getItem(): Observable<any> {
    return this.apiGetItem();
  }

  public insert(objectData: any): Observable<IAPIResponse> {
    return this.apiInsert(objectData);
  }

  public update(objectData: any): Observable<IAPIResponse> {
    this.setExtendedServiceData(objectData.id.toString());
    return this.apiUpdate(objectData);
  }

  public patch(objectData: any): Observable<IAPIResponse> {
    return this.apiPatch(objectData);
  }

  public delete(): Observable<IAPIResponse> {
    return this.apiDelete();
  }

  public custom<T>(url: string, method: string, bodyData: any): Observable<T> {
    return this.apiCustom(url, method, bodyData);
  }
}
