<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaConceptoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_concepto', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('codigo', 10);
            $table->string('descripcion', 80);
            $table->string('codigo_clase_nomina', 10);
            $table->decimal('factor_multiplicador', 18, 4);
            $table->decimal('cantidad_defecto', 18, 4);
            $table->boolean('acumulable');
            $table->text('observaciones');
            $table->boolean('formula');
            $table->string('codigo_formula', 20);
            $table->timestamps();
            $table->primary(['id_usuario', 'codigo', 'codigo_clase_nomina']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_concepto');
    }
}
