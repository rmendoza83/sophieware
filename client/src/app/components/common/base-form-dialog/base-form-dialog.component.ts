import { Component, Inject } from '@angular/core';
import { FORM_DIALOG_TYPE } from 'src/app/constants/form-dialog-type';
import { FieldDefinitionModel } from 'src/app/models/field-definition-model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IFormDialogData } from 'src/app/models/interfaces/form-dialog-data-interface';
import { IDialogResult } from 'src/app/models/interfaces/dialog-result-interface';
import { BaseFormService } from 'src/app/services/common/base-form.service';
import { Observable } from 'rxjs';
import { IAPIResponse } from 'src/app/models/interfaces/api-response-interface';
import { API_RESPONSE } from 'src/app/constants/api-response';

@Component({
  selector: 'app-base-form-dialog',
  templateUrl: './base-form-dialog.component.html',
  styleUrls: ['./base-form-dialog.component.scss']
})
export class BaseFormDialogComponent {
  formDialogType: string = FORM_DIALOG_TYPE.VIEW;
  typeFormDialogType: any = FORM_DIALOG_TYPE;
  formTitle: string = '';
  fields: FieldDefinitionModel[];
  formData: any = {};
  private newFormData: any = {};
  private httpDialogService: BaseFormService;

  constructor (
    private dialogRef: MatDialogRef<BaseFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private dialogData: IFormDialogData) {
    this.formTitle = this.dialogData.formDialogTitle;
    this.formDialogType = this.dialogData.formDialogType;
    this.fields = this.dialogData.formFields;
    this.httpDialogService = this.dialogData.formHttpService;
    if (this.formDialogType !== FORM_DIALOG_TYPE.NEW) {
      this.formData = this.dialogData.formCurrentData;
    }
  }

  validateForm(): boolean {
    return true || this.formDialogType == FORM_DIALOG_TYPE.VIEW;
  }

  onOkClick() {
    if (this.validateForm()) {
      console.log('Ok');
      const result: IDialogResult = {
        submitted: (this.formDialogType != FORM_DIALOG_TYPE.VIEW),
        data: (this.formDialogType != FORM_DIALOG_TYPE.VIEW) ? this.formData : null
      };
      if ([FORM_DIALOG_TYPE.NEW, FORM_DIALOG_TYPE.UPDATE].includes(this.formDialogType)) {
        this.callHttpService()
          .subscribe(response => {
            if (response.statusCode == API_RESPONSE.OK) {
              this.dialogRef.close(result);
            } else {
              console.log(response);
            }
          }); 
      } else {
        this.dialogRef.close(result);
      }
    }
  }

  callHttpService(): Observable<IAPIResponse> {
    if (this.formDialogType == FORM_DIALOG_TYPE.NEW) {
      return this.httpDialogService.insert(this.newFormData);
    }
    return this.httpDialogService.update(this.newFormData);
  }

  onCancelClick() {
    console.log('Cancel');
    const result: IDialogResult = {
       submitted: false,
       data: null
    };
    this.dialogRef.close(result);
  }

  dataChanged(event) {
    this.newFormData = event;
  }
}
