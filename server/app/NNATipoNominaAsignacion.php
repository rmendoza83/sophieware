<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNATipoNominaAsignacion extends Model {

    protected $table = 'nna_tipo_nomina_asignacion';

    protected $fillable = [
        'id_usuario',
        'codigo_tipo_nomina',
        'codigo_concepto',
        'orden',
        'activo'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo_tipo_nomina' => 'required',
        'codigo_concepto' => 'required',
        'orden' => 'required',
        'activo' => 'required'
    ];

}
