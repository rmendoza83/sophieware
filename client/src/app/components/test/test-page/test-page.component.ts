import { Component, OnInit } from '@angular/core';
import { FieldDefinitionModel } from 'src/app/models/field-definition-model';
import { FIELD_TYPES } from 'src/app/constants/field-types';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.css']
})
export class TestPageComponent implements OnInit {
  fields: FieldDefinitionModel[] = [];
  formTitle: string = "El titulo";

  constructor () {
    this.fields.push(new FieldDefinitionModel('login_usuario', 'Login', FIELD_TYPES.STRING, null, null, null, true));
    this.fields.push(new FieldDefinitionModel('nombres', 'Nombres', FIELD_TYPES.STRING, null, null, null, true));
    this.fields.push(new FieldDefinitionModel('apellidos', 'Apellidos', FIELD_TYPES.STRING, null, null, null, true));
    this.fields.push(new FieldDefinitionModel('password_usuario', 'Password', FIELD_TYPES.STRING, null, null, null, false));
    this.fields.push(new FieldDefinitionModel('email', 'E-Mail', FIELD_TYPES.STRING, null, null, null, true));
    this.fields.push(new FieldDefinitionModel('fecha_ingreso', 'Fecha de Ingreso', FIELD_TYPES.DATE, null, null, null, false));
    /*
    this.fields.push(new FieldDefinitionModel('nombre', 'El Nombre', FIELD_TYPES.STRING, null, null, null));
    this.fields.push(new FieldDefinitionModel('apellido', 'El Apellido', FIELD_TYPES.STRING, null, 'danger', 'width: 300px; background-color: red'));
    this.fields.push(new FieldDefinitionModel('memoField', 'El Memo', FIELD_TYPES.TEXT, null, null, null));
    this.fields.push(new FieldDefinitionModel('booleanField', 'El Boolean', FIELD_TYPES.BOOLEAN, null, null, null));
    this.fields.push(new FieldDefinitionModel('integerField', 'El Integer', FIELD_TYPES.INTEGER, null, null, null));
    this.fields.push(new FieldDefinitionModel('decimalField', 'El Decimal', FIELD_TYPES.DECIMAL, null, null, null));
    this.fields.push(new FieldDefinitionModel('moneyField', 'El Money', FIELD_TYPES.MONEY, null, null, null));
    this.fields.push(new FieldDefinitionModel('dateField', 'El Date', FIELD_TYPES.DATE, null, null, null));
    this.fields.push(new FieldDefinitionModel('timeField', 'El time', FIELD_TYPES.TIME, null, null, null));
    this.fields.push(new FieldDefinitionModel('datetimeField', 'El Datetime', FIELD_TYPES.DATETIME, null, null, null));
    */
  }

  ngOnInit() {
  }

}
