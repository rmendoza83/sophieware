<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaTipoNominaDeduccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_tipo_nomina_deduccion', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('codigo_tipo_nomina', 10);
            $table->string('codigo_concepto', 10);
            $table->integer('orden');
            $table->boolean('activo');
            $table->timestamps();
            $table->primary(['id_usuario', 'codigo_tipo_nomina', 'codigo_concepto']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_tipo_nomina_deduccion');
    }
}
