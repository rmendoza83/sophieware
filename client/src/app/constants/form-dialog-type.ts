export class FORM_DIALOG_TYPE {
  public static VIEW = 'view';
  public static NEW = 'new';
  public static UPDATE = 'update';
}