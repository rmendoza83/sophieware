<?php

namespace App\Http\Controllers;

use App\NNAEmpleado;
use Utils\APIResponseResult;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class NNAEmpleadosController extends GenericController {

  public function getlist($id_usuario) {
    return APIResponseResult::OK(NNAEmpleado::where('id_usuario',$id_usuario)->orderBy('id')->get());
  }

  public function get($id) {
    $model = NNAEmpleado::find($id);
    if ($model) {
      return APIResponseResult::OK($model);
    }
    return APIResponseResult::ERROR('Empleado '.$id.' no existe.');
  }

  public function insert(Request $request) {
    $existingModel = NNAEmpleado::where('id_usuario', $request->id_usuario)
      ->where('numero_personal',$request->numero_personal)
      ->first();
    if (!$existingModel) {
      try {
        $model = new NNAEmpleado;
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Ya existe un Empleado con el numero de personal especificado.');
  }

  public function update($id, Request $request) {
    $model = NNAEmpleado::find($id);
    if ($model) {
      try {
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK();
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Empleado '.$id.' no existe. No se puede actualizar el empleado.');
  }

  public function delete($id) {
    $model = NNAEmpleado::find($id);
    if ($model) {
      try {
        $model->delete();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Empleado '.$id.' no existe. No se puede eliminar el empleado.');
  }
}
