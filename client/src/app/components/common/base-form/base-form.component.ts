import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseFormService } from 'src/app/services/common/base-form.service';
import { FieldDefinitionModel } from 'src/app/models/field-definition-model';
import { FIELD_TYPES } from 'src/app/constants/field-types';
import { FORM_DIALOG_TYPE } from 'src/app/constants/form-dialog-type';

import * as moment from 'moment';

@Component({
  selector: 'app-base-form',
  templateUrl: './base-form.component.html',
  styleUrls: ['./base-form.component.css']
})
export class BaseFormComponent implements OnInit {
  formTitle: string;
  formDialogType: string;
  fields: FieldDefinitionModel[];
  typeField: any = FIELD_TYPES;
  typeFormDialogType: any = FORM_DIALOG_TYPE;
  formData: any = {};
  //PrimeNG Calendar
  private es: any;
  private defaultDate: Date;

  @Input() set iFields(fields: FieldDefinitionModel[]) {
    this.fields = fields;
  }

  @Input() set iFormTitle(formTitle: string) {
    this.formTitle = formTitle;
  }

  @Input() set iFormDialogType(formDialogType: string) {
    this.formDialogType = formDialogType;
  }

  @Input() set iFormData(formData: any) {
    this.formData = formData;
    // Parsing Date Fields
    this.fields.forEach(field => {
      if ([FIELD_TYPES.DATE, FIELD_TYPES.TIME, FIELD_TYPES.TIME].includes(field.datatype)) {
        this.formData[field.fieldName] = moment(formData[field.fieldName]).toDate();
      }
    });
  }

  @Output() oFormData: EventEmitter<any> = new EventEmitter<any>();

  constructor () { }

  ngOnInit() {
    this.es = {
      firstDayOfWeek: 1,
      dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
      dayNamesShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
      dayNamesMin: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
      monthNames: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
      monthNamesShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
      today: 'Hoy',
      clear: 'Borrar'
    }
    this.defaultDate = moment().toDate();
    if (this.formDialogType == FORM_DIALOG_TYPE.NEW) {
      this.fields.forEach(field => {
        if (this.isDateTimeField(field)) {
          this.formData[field.fieldName] = this.defaultDate;
        }
      });
    }
  }

  isDateTimeField(field: FieldDefinitionModel) {
    return ([FIELD_TYPES.DATE, FIELD_TYPES.TIME, FIELD_TYPES.DATETIME].includes(field.datatype));
  }

  private parseFormData(): any {
    let newFormData = JSON.parse(JSON.stringify(this.formData));
    //Copiando valores del objeto, parseando Fechas y Horas
    for (let propertie in this.formData) {
      const field = this.fields.find(item => {
        return (item.fieldName == propertie);
      });
      if (field && this.isDateTimeField(field)) {
        newFormData[field.fieldName] = moment(this.formData[field.fieldName]).format("YYYY-MM-DD hh:mm:ss");
      }
    }
    return newFormData;
  }

  dataChanged() {
    this.oFormData.emit(this.parseFormData());
  }
}
