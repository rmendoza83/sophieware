<?php

use Faker\Generator as Faker;
use Illuminate\Hashing\BcryptHasher;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\SYSUsuario::class, function (Faker $faker) {
  $firstName = $faker->firstName();
  $lastName = $faker->lastName();
  $loginUsuario = strtolower(substr($firstName, 0, 1) . $lastName);
  return [
    'login_usuario' => $loginUsuario,
    'nombres' => $firstName,
    'apellidos' => $lastName,
    'password_usuario' => (new BcryptHasher)->make($loginUsuario),
    'email' => $faker->email,
    'fecha_ingreso' => $faker->date('Y-m-d')
  ];
});
