<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAVariableCalculo extends Model {

    protected $table = 'nna_variable_calculo';

    protected $fillable = [
        'id_usuario',
        'codigo',
        'descripcion',
        'constante',
        'valor_constante',
        'variable',
        'tipo_variable',
        'observaciones'
    ];

    protected $dates = ['fecha_ingreso'];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo' => 'required',
        'descripcion' => 'required',
        'constante' => 'required',
        'valor_constante' => 'required',
        'variable' => 'required',
        'tipo_variable' => 'required',
        'observaciones' => 'required'
    ];

}
