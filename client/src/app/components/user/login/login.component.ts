import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import { SYSUsuarioModel } from 'src/app/models/sys-usuario-model';
import { API_RESPONSE } from 'src/app/constants/api-response';
import { APISecurityTokenModel } from 'src/app/models/api-security-token-model';
import { SESSION_CONSTANTS } from 'src/app/constants/session-constants';
import * as moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public email: string;
  public password: string;
  private model: SYSUsuarioModel;
  constructor (
    private router: Router,
    private loginService: LoginService
  ) { }

  doLogin() {
    if (this.email && this.password) {
      this.model = new SYSUsuarioModel();
      this.model.email = this.email;
      this.model.password_usuario = this.password;
      this.loginService.doLogin(this.model)
        .subscribe(response => {
          if (response.statusCode == API_RESPONSE.OK) {
            //Redirect and register session
            const securityToken: APISecurityTokenModel = response.data as APISecurityTokenModel;
            console.log(securityToken);
            localStorage.clear();
            localStorage.setItem(SESSION_CONSTANTS.USERNAME,this.email);
            localStorage.setItem(SESSION_CONSTANTS.SECURITY_TOKEN,securityToken.access_token);
            localStorage.setItem(SESSION_CONSTANTS.TOKEN_EXPIRES_IN,securityToken.expires_in.toString());
            localStorage.setItem(SESSION_CONSTANTS.TOKEN_TYPE,securityToken.token_type);
            localStorage.setItem(SESSION_CONSTANTS.TOKEN_TIMESTAMP, moment().unix().toString());
            console.log(moment().unix());
            this.router.navigateByUrl('main-menu');
          }
        });
    }
  }
}
