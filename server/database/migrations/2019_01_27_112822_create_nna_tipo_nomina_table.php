<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaTipoNominaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_tipo_nomina', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('codigo', 10);
            $table->string('descripcion', 40);
            $table->string('frecuencia_pago', 10);
            $table->boolean('confidencial');
            $table->text('observaciones');
            $table->timestamps();
            $table->primary(['id_usuario', 'codigo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_tipo_nomina');
    }
}
