<?php

namespace Constants;

class ApiMethods
{
    const GET_ITEM = 'get';
    const GET_LIST = 'getlist';
    const INSERT = 'insert';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const DELETE_MANY = 'deletemany';
}

?>