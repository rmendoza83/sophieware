<?php

use Constants\ApiMethods as API_METHODS;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
function defineBaseMethods($router, $module, $table, $controller, $general = false) {
    $url = ($general) ? $module.'/'.$table : $module.'/'.$table.'/{id_usuario}';
    $router->get($url,$controller.'@'.API_METHODS::GET_LIST);
    $router->get($url.'/{id}',$controller.'@'.API_METHODS::GET_ITEM);
    $router->post($url,$controller.'@'.API_METHODS::INSERT);
    $router->put($url.'/{id}',$controller.'@'.API_METHODS::UPDATE);
    $router->delete($url.'/{id}',$controller.'@'.API_METHODS::DELETE);
}

// Authentication rules
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('login/login', 'AuthController@login');
    $router->get('login/refresh', 'AuthController@refresh');
    $router->get('login/getCurrentUser', 'AuthController@current_user');
    $router->get('login/logout', 'AuthController@logout');
});

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
    // Sistema Usuarios
    defineBaseMethods($router, 'sys', 'usuario', 'SYSUsuarioController', true);
    // General Tipo de Identificacion
    defineBaseMethods($router, 'gen', 'tipo_identificacion', 'GenTipoIdentificacionController');
    // Nomina Empleados
    defineBaseMethods($router, 'nna', 'empleado', 'NNAEmpleadoController');
    // Nomina Posiciones
    defineBaseMethods($router, 'nna', 'posicion', 'NNAPosicionController');
    // Nomina Departamentos
    defineBaseMethods($router, 'nna', 'departamento', 'NNADepartamentoController');
    // Nomina Clases de Nomina
    defineBaseMethods($router, 'nna', 'clase_nomina', 'NNAClaseNominaController');
    // Nomina Conceptos
    defineBaseMethods($router, 'nna', 'concepto', 'NNAConceptoController');
    // Nomina Tipos de Nomina
    defineBaseMethods($router, 'nna', 'tipo_nomina', 'NNATipoNominaController');
    // Nomina Asignaciones de Tipos de Nomina
    defineBaseMethods($router, 'nna', 'tipo_nomina_asignacion', 'NNATipoNominaAsignacionController');
    // Nomina Deducciones de Tipos de Nomina
    defineBaseMethods($router, 'nna', 'tipo_nomina_deduccion', 'NNATipoNominaDeduccionController');
    // Nomina Variables de Calculo
    defineBaseMethods($router, 'nna', 'variable_calculo', 'NNAVariableCalculoController');
    // Nomina Formulas
    defineBaseMethods($router, 'nna', 'formula', 'NNAFormulaController');
    // Nomina Dias no Laborables
    defineBaseMethods($router, 'nna', 'dia_no_laborable', 'NNAEmpleadoController');
    // Nomina Pagos de Nomina
    defineBaseMethods($router, 'nna', 'pago_nomina', 'NNAPagoNominaController');
    // Nomina Asignaciones de Pagos de Nomina
    defineBaseMethods($router, 'nna', 'pago_nomina_asignacion', 'NNAPagoNominaAsignacionController');
    // Nomina Deducciones de Pagos de Nomina
    defineBaseMethods($router, 'nna', 'pago_nomina_deduccion', 'NNAPagoNominaDeduccionController');
    ///////////////////////
    // Rutas adicionales //
    ///////////////////////
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});
