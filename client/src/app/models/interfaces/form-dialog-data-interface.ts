import { FieldDefinitionModel } from '../field-definition-model';
import { BaseFormService } from 'src/app/services/common/base-form.service';

export interface IFormDialogData {
  formDialogType: string;
  formDialogTitle: string;
  formFields: FieldDefinitionModel[];
  formHttpService: BaseFormService;
  formCurrentData: any;
}