<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Hashing\BcryptHasher;

class SYSUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_usuario')->delete();
        DB::table('sys_usuario')->insert([
            'login_usuario' => 'admin',
            'nombres' => 'Administrador',
            'apellidos' => 'Sistema',
            'password_usuario' => (new BcryptHasher)->make('admin'),
            'email' => 'admin@sophie-ware.com',
            'fecha_ingreso' => date("Y-m-d")
        ]);
        factory(App\SYSUsuario::class, 1000)->create();
    }
}
