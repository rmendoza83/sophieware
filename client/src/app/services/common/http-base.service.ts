import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API_CONSTANTS } from 'src/app/constants/api-constants';
import { APISecurityTokenModel } from 'src/app/models/api-security-token-model';
import { HTTP_CONSTANTS } from 'src/app/constants/http-constants';

@Injectable({
  providedIn: 'root'
})
export abstract class HTTPBaseService {
  protected readonly serverAPIURL: string = 'https://test.sophie-ware.com/api/';
  // protected readonly serverAPIURL: string = 'http://localhost:8000/api/';
  protected moduleName: string = '';
  protected serviceName: string = '';
  protected extendedServiceData: string = '';
  protected APISecurityToken: APISecurityTokenModel;

  constructor (
    private httpClient: HttpClient) {
  }
  
  private getHTTPHeaders(): HttpHeaders {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    if (this.hasAPISecurityToken()) {
      httpHeaders = httpHeaders.append('Authorization',this.APISecurityToken.token_type + ' ' + this.APISecurityToken.access_token);
    }
    return httpHeaders;
  }

  private hasAPISecurityToken(): boolean {
    return (this.APISecurityToken) && (this.APISecurityToken.access_token) && (this.APISecurityToken.access_token.length > 0);
  }

  private getHTTPOptions() {
    return {
      headers: this.getHTTPHeaders()
    };
  }

  private doHTTPCall<T>(url: string, method: string, bodyData: any): Observable<T> {
    var http = this.httpClient;
    var options = this.getHTTPOptions();

    switch (method) {
      case HTTP_CONSTANTS.GET:
        return http.get<T>(url,options);
      case HTTP_CONSTANTS.POST:
        return http.post<T>(url,bodyData,options);
      case HTTP_CONSTANTS.PUT:
        return http.put<T>(url,bodyData,options);
      case HTTP_CONSTANTS.PATCH:
        return http.patch<T>(url,bodyData,options);
      case HTTP_CONSTANTS.DELETE:
        return http.delete<T>(url,options);
    }
    return null;
  }

  private doGET<T>(url: string): Observable<T> {
    return this.doHTTPCall<T>(url, HTTP_CONSTANTS.GET, null);
  }

  private doPOST<T>(url: string, bodyData: any): Observable<T> {
    return this.doHTTPCall<T>(url, HTTP_CONSTANTS.POST, bodyData);
  }

  private doPUT<T>(url: string, bodyData: any): Observable<T> {
    return this.doHTTPCall<T>(url, HTTP_CONSTANTS.PUT, bodyData);
  }

  private doPATCH<T>(url: string, bodyData: any): Observable<T> {
    return this.doHTTPCall<T>(url, HTTP_CONSTANTS.PATCH, bodyData);
  }

  private doDELETE<T>(url: string): Observable<T> {
    return this.doHTTPCall<T>(url, HTTP_CONSTANTS.DELETE, null);
  }

  private getURL(): string {
    let url = this.serverAPIURL + this.moduleName + '/' + this.serviceName;
    if (this.extendedServiceData && this.extendedServiceData.length > 0) {
      url += '/' + this.extendedServiceData;
      this.extendedServiceData = '';
    }
    return url;
  }

  protected setExtendedServiceData(data: string) {
    this.extendedServiceData = data;
  }

  protected apiGetList<T>(): Observable<T> {
    return this.doGET(this.getURL());
  }

  protected apiGetItem<T>(): Observable<T> {
    return this.doGET<T>(this.getURL());
  }

  protected apiInsert<T>(objectData: any): Observable<T> {
    return this.doPOST<T>(this.getURL(), objectData);
  }

  protected apiUpdate<T>(objectData: any): Observable<T> {
    return this.doPUT<T>(this.getURL(), objectData);
  }

  protected apiPatch<T>(objectData: any): Observable<T> {
    return this.doPATCH<T>(this.getURL(), objectData);
  }

  protected apiDelete<T>(): Observable<T> {
    return this.doDELETE<T>(this.getURL());
  }

  protected apiDeleteAll<T>(): Observable<T> {
    return this.doDELETE<T>(this.getURL());
  }

  protected apiCustom<T>(url: string, method: string, bodyData: any): Observable<T> {
    return this.doHTTPCall<T>(url, method, bodyData);
  }
}
