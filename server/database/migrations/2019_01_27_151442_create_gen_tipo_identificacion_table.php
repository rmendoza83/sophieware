<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenTipoIdentificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_tipo_identificacion', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->string('codigo', 10);
            $table->char('inicial');
            $table->string('codigo_corto', 3);
            $table->string('descripcion', 40);
            $table->timestamps();
            $table->primary(['codigo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_tipo_identificacion');
    }
}
