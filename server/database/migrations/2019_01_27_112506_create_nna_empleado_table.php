<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_empleado', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('numero_personal');
            $table->string('nombres', 40);
            $table->string('apellidos', 40);
            $table->integer('codigo_tipo_identificacion');
            $table->string('nro_identificacion', 20);
            $table->date('fecha_ingreso');
            $table->char('sexo', 1);
            $table->integer('id_departamento');
            $table->integer('id_posicion');
            $table->decimal('salario_base', 18, 4);
            $table->decimal('hora_base', 18, 4);
            $table->text('observaciones');
            $table->timestamps();
            $table->primary(['id_usuario', 'numero_personal']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_empleado');
    }
}
