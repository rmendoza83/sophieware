<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class GenericController extends Controller
{
  protected function fillModel(Model $model, Request $request)
  {
    foreach ($model->getFillable() as $field)
    {
      $model->$field = $request->$field;
    }
  }
  /*
    GetList method controller
  */
  abstract public function getlist();
  /*
    GetItem method controller
  */
  abstract public function get();
  /*
    Insert method controller
  */
  abstract public function insert();
  /*
    Update method controller
  */
  abstract public function update();
  /*
    Delete method controller
  */
  abstract public function delete();
}