import { IMenuItem } from './menu-item-interface';

export interface IMenuGroup {
  title: string;
  icon: string;
  items: IMenuItem[];
}