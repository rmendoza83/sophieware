import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { HTTPBaseService } from './http-base.service';

import { IAPIResponse } from 'src/app/models/interfaces/api-response-interface';
import { APISecurityTokenModel } from 'src/app/models/api-security-token-model';

@Injectable({
  providedIn: 'root'
})
export class BaseTableService extends HTTPBaseService {

  constructor (
    private http: HttpClient
  ) {
    super(http);
  }

  public setModuleName(moduleName: string) {
    this.moduleName = moduleName;
  }

  public setServiceName(serviceName: string) {
    this.serviceName = serviceName;
  }

  public setAPISecurityToken(APISecurityToken: APISecurityTokenModel) {
    this.APISecurityToken = APISecurityToken;
  }

  public getList(): Observable<IAPIResponse> {
    return this.apiGetList<IAPIResponse>();
  }

  public getItem(item: string): Observable<IAPIResponse> {
    this.setExtendedServiceData(item);
    return this.apiGetItem<IAPIResponse>();
  }

  public insert(model: any): Observable<IAPIResponse> {
    return this.apiInsert<IAPIResponse>(model);
  }

  public update(model: any): Observable<IAPIResponse> {
    return this.apiUpdate<IAPIResponse>(model);
  }

  public delete(item: string): Observable<IAPIResponse> {
    this.setExtendedServiceData(item);
    return this.apiDelete();
  }
}
