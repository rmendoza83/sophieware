<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNATipoNomina extends Model {

    protected $table = 'nna_tipo_nomina';

    protected $fillable = [
        'id_usuario',
        'codigo',
        'descripcion',
        'frecuencia_pago',
        'confidencial',
        'observaciones'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo' => 'required',
        'descripcion' => 'required',
        'frecuencia_pago' => 'required',
        'confidencial' => 'required',
        'observaciones' => 'required'
    ];

}
