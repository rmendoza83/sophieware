<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->string('login_usuario', 40);
            $table->string('nombres', 40);
            $table->string('apellidos', 40);
            $table->string('password_usuario', 100);
            $table->string('email', 80);
            $table->date('fecha_ingreso');
            $table->timestamps();
            $table->primary(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_usuario');
    }
}
