export interface IAPIResponse {
    statusCode: string;
    data: any;
}