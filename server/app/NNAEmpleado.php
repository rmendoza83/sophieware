<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAEmpleado extends Model {

    protected $table = 'nna_empleado';

    protected $fillable = [
        'id_usuario',
        'numero_personal',
        'nombres',
        'apellidos',
        'codigo_tipo_identificacion',
        'nro_identificacion',
        'fecha_ingreso',
        'sexo',
        'id_departamento',
        'id_posicion',
        'salario_base',
        'hora_base',
        'observaciones'
    ];

    protected $dates = ['fecha_ingreso'];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'numero_personal' => 'required',
        'nombres' => 'required',
        'apellidos' => 'required',
        'codigo_tipo_identificacion' => 'required',
        'nro_identificacion' => 'required',
        'fecha_ingreso' => 'required',
        'sexo' => 'required',
        'id_departamento' => 'required',
        'id_posicion' => 'required',
        'salario_base' => 'required',
        'hora_base' => 'required',
        'observaciones' => 'required'
    ];

}
