<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaPagoNominaAsignacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_pago_nomina_asignacion', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->integer('id_nomina_pago_nomina');
            $table->string('numero_personal', 10);
            $table->string('codigo_concepto', 10);
            $table->string('descripcion', 80);
            $table->decimal('cantidad', 18, 4);
            $table->decimal('monto_base', 18, 4);
            $table->decimal('monto_total', 18, 4);
            $table->text('observaciones');
            $table->timestamps();
            $table->primary(['id_usuario', 'id_nomina_pago_nomina', 'numero_personal']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_pago_nomina_asignacion');
    }
}
