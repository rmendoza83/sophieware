import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IMenuGroup } from 'src/app/models/interfaces/menu-group-interface';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent {
  menuGroups: IMenuGroup[];

  constructor (
    private router: Router
  ) {
    this.menuGroups = [
      {
        title: 'Testing Purpose',
        icon: null,
        items: [
          { caption: 'Usuarios', icon: null, route: 'test'}
        ]
      },
      {
        title: 'General',
        icon: null,
        items: [
          { caption: 'Dashboard', icon: 'dashboard', route: 'nna-dashboard' }
        ]
      },
      {
        title: 'Nomina',
        icon: null,
        items: [
          { caption: 'Pagos de Nomina', icon: null, route: 'nna-pago-nomina' },
          { caption: 'Tipos de Nomina', icon: null, route: 'nna-tipo-nomina' },
          { caption: 'Clases de Nomina', icon: null, route: 'nna-clase-nomina' }
        ]
      },
      {
        title: 'Maestros',
        icon: null,
        items: [
          { caption: 'Empleados', icon: null, route: 'nna-empleado' },
          { caption: 'Departamentos', icon: null, route: 'nna-departamento' },
          { caption: 'Posiciones', icon: null, route: 'nna-posicion' }
        ]
      },
      {
        title: 'Configuracion',
        icon: null,
        items: [
          { caption: 'Conceptos de Pago', icon: null, route: 'nna-concepto' },
          { caption: 'Formulas', icon: null, route: 'nna-formula' },
          { caption: 'Variables de Calculo', icon: null, route: 'nna-variable-calculo' },
          { caption: 'Dias No Laborables', icon: null, route: 'nna-dia-no-laborable' }
        ]
      },
      {
        title: 'Reportes',
        icon: null,
        items: [
          { caption: 'Reportes Generales', icon: null, route: 'report' }
        ]
      }
    ]
  }

  changePassword() {
    console.log('change password event');
  }

  exit() {
    console.log('exit event');
    //Cerrar Sesion
    this.router.navigate(['/login']);
  }
}
