import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { MessageDialogComponent } from 'src/app/components/common/message-dialog/message-dialog.component';
import { IMessageDialogData } from 'src/app/models/interfaces/message-dialog-data-interface';
import { MESSAGE_DIALOG_TYPE } from 'src/app/constants/message-dialog-type';

@Injectable({
  providedIn: 'root'
})
export class MessageDialogService {
  private messageDialogData: IMessageDialogData = {
    messageDialogType: MESSAGE_DIALOG_TYPE.INFORMATION,
    messageDialogTitle: null,
    messageDialogMessage: null
  };

  constructor (
    private dialog: MatDialog
  ) { }

  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.messageDialogData.messageDialogMessage = 'Mensaje de prueba';
    this.messageDialogData.messageDialogTitle = 'Titulo de prueba';
    this.messageDialogData.messageDialogType = MESSAGE_DIALOG_TYPE.CONFIRMATION;
    dialogConfig.data = this.messageDialogData;

    const dialogRef = this.dialog.open(MessageDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => console.log("Dialog output:", data)
    );
  }
}
