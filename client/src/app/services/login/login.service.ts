import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { HTTPBaseService } from '../common/http-base.service';
import { SYSUsuarioModel } from 'src/app/models/sys-usuario-model';
import { HTTP_CONSTANTS } from 'src/app/constants/http-constants';
import { IAPIResponse } from 'src/app/models/interfaces/api-response-interface';


@Injectable({
  providedIn: 'root'
})
export class LoginService extends HTTPBaseService {

  constructor (
    http: HttpClient
  ) {
    super(http);
    this.APISecurityToken = null;
  }

  doLogin(data: SYSUsuarioModel): Observable<IAPIResponse> {
    const url = this.serverAPIURL + "login/login";
    return this.apiCustom<IAPIResponse>(url, HTTP_CONSTANTS.POST, data);
  }
}
