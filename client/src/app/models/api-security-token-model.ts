import { SESSION_CONSTANTS } from 'src/app/constants/session-constants';

export class APISecurityTokenModel {
  username: string;
  access_token: string;
  token_type: string;
  expires_in: number;
  timestamp: number;

  public static getSessionData(): APISecurityTokenModel {
    const username = localStorage.getItem(SESSION_CONSTANTS.USERNAME);
    const access_token = localStorage.getItem(SESSION_CONSTANTS.SECURITY_TOKEN);
    const token_type = localStorage.getItem(SESSION_CONSTANTS.TOKEN_TYPE);
    const expires_in = localStorage.getItem(SESSION_CONSTANTS.TOKEN_EXPIRES_IN);
    const timestamp = localStorage.getItem(SESSION_CONSTANTS.TOKEN_TIMESTAMP);
    if (username && access_token && token_type && expires_in && timestamp) {
      return {
        username: username,
        access_token: access_token,
        token_type: token_type,
        expires_in: Number(expires_in),
        timestamp: Number(timestamp)
      };
    }
    return null;
  }
}