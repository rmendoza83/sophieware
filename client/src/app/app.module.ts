import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatDividerModule, MatMenuModule } from '@angular/material';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { KeyFilterModule } from 'primeng/keyfilter';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { ButtonModule } from 'primeng/button';
import { PasswordModule } from 'primeng/password';

// DevExpress Modules Importation
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxButtonModule } from 'devextreme-angular/ui/button';

// Custom Components
import { BaseFormComponent } from './components/common/base-form/base-form.component';
import { TestPageComponent } from './components/test/test-page/test-page.component';
import { LoginComponent } from './components/user/login/login.component';
import { BaseTableComponent } from './components/common/base-table/base-table.component';
import { MainComponent } from './components/main/main/main.component';
import { MessageDialogComponent } from './components/common/message-dialog/message-dialog.component';
import { BaseFormDialogComponent } from './components/common/base-form-dialog/base-form-dialog.component';
import { DashboardComponent } from './components/nomina/dashboard/dashboard.component';
import { MainMenuComponent } from './components/main/main-menu/main-menu.component';
import { PagoNominaComponent } from './components/nomina/pago-nomina/pago-nomina.component';
import { TipoNominaComponent } from './components/nomina/tipo-nomina/tipo-nomina.component';
import { ClaseNominaComponent } from './components/nomina/clase-nomina/clase-nomina.component';
import { EmpleadoComponent } from './components/nomina/empleado/empleado.component';
import { DepartamentoComponent } from './components/nomina/departamento/departamento.component';
import { PosicionComponent } from './components/nomina/posicion/posicion.component';
import { ConceptoComponent } from './components/nomina/concepto/concepto.component';
import { FormulaComponent } from './components/nomina/formula/formula.component';
import { VariableCalculoComponent } from './components/nomina/variable-calculo/variable-calculo.component';
import { DiaNoLaborableComponent } from './components/nomina/dia-no-laborable/dia-no-laborable.component';
import { NominaReportComponent } from './components/main/nomina-report/nomina-report.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseFormComponent,
    TestPageComponent,
    LoginComponent,
    MainComponent,
    BaseTableComponent,
    MessageDialogComponent,
    BaseFormDialogComponent,
    DashboardComponent,
    MainMenuComponent,
    PagoNominaComponent,
    TipoNominaComponent,
    ClaseNominaComponent,
    EmpleadoComponent,
    DepartamentoComponent,
    PosicionComponent,
    ConceptoComponent,
    FormulaComponent,
    VariableCalculoComponent,
    DiaNoLaborableComponent,
    NominaReportComponent
  ],
  entryComponents: [
    MessageDialogComponent,
    BaseFormDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // Angular Material Modules
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatDividerModule,
    MatMenuModule,
    // PrimeNG Modules
    KeyFilterModule,
    InputTextModule,
    InputTextareaModule,
    CheckboxModule,
    DropdownModule,
    CalendarModule,
    ButtonModule,
    PasswordModule,
    // Devextreme Modules
    DxDataGridModule,
    DxButtonModule
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false} },
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: {} }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
