<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNADiaNoLaborable extends Model {

    protected $table = 'nna_dia_no_laborable';

    protected $fillable = [
        'id_usuario',
        'dia',
        'mes',
        'anno',
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'dia' => 'required',
        'mes' => 'required',
        'anno' => 'required'
    ];

}
