<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenTipoIdentificacionSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('gen_tipo_identificacion')->delete();
    DB::table('gen_tipo_identificacion')->insert([
      'codigo' => 'CEDULA',
      'inicial' => 'CEDULA',
      'codigo_corto' => 'CED',
      'descripcion' => 'CEDULA'
    ]);
    DB::table('gen_tipo_identificacion')->insert([
      'codigo' => 'RUC',
      'inicial' => 'RUC',
      'codigo_corto' => 'RUC',
      'descripcion' => 'REGISTRO UNICO DE CONTRIBUYENTE'
    ]);
    DB::table('gen_tipo_identificacion')->insert([
      'codigo' => 'RISE',
      'inicial' => 'RISE',
      'codigo_corto' => 'RIS',
      'descripcion' => 'REGISTRO IMPOSITIVO SIMPLICADO'
    ]);
  }
}
