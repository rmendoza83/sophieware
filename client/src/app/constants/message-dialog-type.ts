export class MESSAGE_DIALOG_TYPE {
  public static INFORMATION = 'info';
  public static CONFIRMATION = 'confirm';
  public static ERROR = 'error';
}