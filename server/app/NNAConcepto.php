<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAConcepto extends Model {

    protected $table = 'nna_concepto';

    protected $fillable = [
        'id_usuario',
        'codigo',
        'descripcion',
        'codigo_clase_nomina',
        'factor_multiplicador',
        'cantidad_defecto',
        'acumulable',
        'observaciones',
        'formula',
        'codigo_formula'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo' => 'required',
        'descripcion' => 'required',
        'codigo_clase_nomina' => 'required',
        'factor_multiplicador' => 'required',
        'cantidad_defecto' => 'required',
        'acumulable' => 'required',
        'observaciones' => 'required',
        'formula' => 'required',
        'codigo_formula' => 'required'
    ];

}
