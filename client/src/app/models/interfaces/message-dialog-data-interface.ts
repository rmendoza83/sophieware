export interface IMessageDialogData {
  messageDialogType: string;
  messageDialogTitle: string;
  messageDialogMessage: string;
}