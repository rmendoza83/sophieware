import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { FieldDefinitionModel } from 'src/app/models/field-definition-model';

import { API_RESPONSE } from 'src/app/constants/api-response';
import { FORM_DIALOG_TYPE } from 'src/app/constants/form-dialog-type';

import { APISecurityTokenModel } from 'src/app/models/api-security-token-model';
import { IFormDialogData } from 'src/app/models/interfaces/form-dialog-data-interface';

import { BaseTableService } from 'src/app/services/common/base-table.service';
import { MessageDialogService } from 'src/app/services/common/message-dialog.service';

import { BaseFormDialogComponent } from '../base-form-dialog/base-form-dialog.component';
import { BaseFormService } from 'src/app/services/common/base-form.service';
import { IDialogResult } from 'src/app/models/interfaces/dialog-result-interface';
import { Subscription } from 'rxjs';
import { FIELD_TYPES } from 'src/app/constants/field-types';

@Component({
  selector: 'app-base-table',
  templateUrl: './base-table.component.html',
  styleUrls: ['./base-table.component.css']
})
export class BaseTableComponent implements OnInit {
  fields: FieldDefinitionModel[];
  fieldsTable: FieldDefinitionModel[];
  dataSource: any[];
  searchText: string;
  private searchSubscription: Subscription;
  private data: any[];
  private formDialogData: IFormDialogData = {
    formDialogType: FORM_DIALOG_TYPE.VIEW,
    formDialogTitle: null,
    formFields: null,
    formHttpService: null,
    formCurrentData: null
  };

  @Input() set iFields(fields: FieldDefinitionModel[]) {
    this.fields = fields;
    this.fieldsTable = fields.filter(item => item.showInTable);
  }

  @Input() set iModuleName(moduleName: string) {
    this.httpTableService.setModuleName(moduleName);
    this.httpFormService.setModuleName(moduleName);
  }

  @Input() set iServiceName(serviceName: string) {
    this.httpTableService.setServiceName(serviceName);
    this.httpFormService.setServiceName(serviceName);
  }

  constructor (
    private httpTableService: BaseTableService,
    private httpFormService: BaseFormService,
    private messageDialogService: MessageDialogService,
    private formDialog: MatDialog
  ) {
    const sessionData = APISecurityTokenModel.getSessionData();
    this.httpTableService.setAPISecurityToken(sessionData);
    this.httpFormService.setAPISecurityToken(sessionData);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    console.log("loading data...");
    this.httpTableService.getList()
      .subscribe(response => {
        if (response.statusCode == API_RESPONSE.OK) {
          console.log('Data Loaded');
          this.dataSource = this.data = response.data as any[];
          this.searchText = '';
        }
      });
  }

  private validateFilter(row: any, stringToSearch: string) {
    let result = false;

    this.fields.forEach(field => {
      if ([FIELD_TYPES.STRING, FIELD_TYPES.TEXT].includes(field.datatype)) {
        if (row[field.fieldName]) {
          result = result || row[field.fieldName].toUpperCase().includes(stringToSearch.toUpperCase());
        }
      }
    });

    return result;
  }

  doSearch() {
    if (this.searchText.trim().length > 0) {
      this.dataSource = this.data.filter(row => {
        return this.validateFilter(row, this.searchText.trim());
      });
    } else {
      this.dataSource = this.data;
    }
  }

  viewRow(data) {
    const dialogConfig = this.getDialogConfig(FORM_DIALOG_TYPE.VIEW, 'Ver Registro', data);
    this.formDialog.open(BaseFormDialogComponent, dialogConfig);
  }

  updateRow(data) {
    const dialogConfig = this.getDialogConfig(FORM_DIALOG_TYPE.UPDATE, 'Editar Registro', data);
    const dialogRef = this.formDialog.open(BaseFormDialogComponent, dialogConfig);

    let subscription = dialogRef.afterClosed()
      .subscribe((data: IDialogResult) => {
        console.log("Dialog output:", data);
        if (data.submitted == true) {
          this.loadData();
        }
        subscription.unsubscribe();
      });
  }

  deleteRow(data) {
    const subscription = this.httpTableService.delete(data.id.toString())
      .subscribe(response => {
        if (response.statusCode == API_RESPONSE.OK) {
          //Mostrar Mensaje
          console.log("Record Deleted");
          this.loadData();
        }
        subscription.unsubscribe();
      });
  }

  showDialog() {
    this.messageDialogService.openDialog();
  }

  onNewClick() {
    const dialogConfig = this.getDialogConfig(FORM_DIALOG_TYPE.NEW, 'Nuevo Registro', null);
    const dialogRef = this.formDialog.open(BaseFormDialogComponent, dialogConfig);

    const subscription = dialogRef.afterClosed()
      .subscribe((data: IDialogResult) => {
        console.log("Dialog output:", data);
        if (data.submitted == true) {
          this.loadData();
        }
        subscription.unsubscribe();
      });
  }

  private getDialogConfig(dialogType: string, title: string, currentData: any): MatDialogConfig {
    let dialogConfig = new MatDialogConfig();
    dialogConfig.width = '640px';
    dialogConfig.height = '480px';
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.formDialogData.formDialogType = dialogType;
    this.formDialogData.formDialogTitle = title;
    this.formDialogData.formFields = this.fields;
    this.formDialogData.formHttpService = this.httpFormService;
    this.formDialogData.formCurrentData = currentData;
    dialogConfig.data = this.formDialogData;

    return dialogConfig;
  }
}
