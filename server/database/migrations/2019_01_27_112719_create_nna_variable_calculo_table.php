<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaVariableCalculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_variable_calculo', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('codigo', 20);
            $table->string('descripcion', 40);
            $table->boolean('constante');
            $table->double('valor_constante', 18, 4);
            $table->boolean('variable');
            $table->string('tipo_variable', 20);
            $table->text('observaciones');
            $table->primary(['id_usuario', 'codigo']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_variable_calculo');
    }
}
