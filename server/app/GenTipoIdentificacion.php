<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GenTipoIdentificacion extends Model {

    protected $table = 'gen_tipo_identificacion';

    protected $fillable = [
        'codigo',
        'inicial',
        'codigo_corto',
        'descripcion',
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'codigo' => 'required',
        'inicial' => 'required',
        'codigo_corto' => 'required',
        'descripcion' => 'required'
    ];

}
