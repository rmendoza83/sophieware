<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class SYSUsuario extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'sys_usuario';

    protected $fillable = ['login_usuario', 'nombres', 'apellidos', 'password_usuario', 'email', 'fecha_ingreso'];

    protected $dates = ['fecha_ingreso'];

    protected $hidden = ['password_usuario'];

    public static $rules = [
        // Validation rules
        'login_usuario' => 'required',
        'nombres' => 'required',
        'apellidos' => 'required',
        'password_usuario' => 'required',
        'email' => 'required',
        'fecha_ingreso' => 'required'
    ];

    // Relationships

    // JWT Implementation
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
        return $this->password_usuario;
    }
}
