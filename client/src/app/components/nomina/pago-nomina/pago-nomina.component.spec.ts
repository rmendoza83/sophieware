import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoNominaComponent } from './pago-nomina.component';

describe('PagoNominaComponent', () => {
  let component: PagoNominaComponent;
  let fixture: ComponentFixture<PagoNominaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoNominaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoNominaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
