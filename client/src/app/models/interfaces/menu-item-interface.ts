export interface IMenuItem {
  caption: string;
  icon: string;
  route: string;
}