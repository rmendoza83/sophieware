<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAFormula extends Model {

    protected $table = 'nna_formula';

    protected $fillable = [
        'id_usuario',
        'codigo',
        'descripcion',
        'factor_multiplicador',
        'texto_formula'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo' => 'required',
        'descripcion' => 'required',
        'factor_multiplicador' => 'required',
        'texto_formula' => 'required'
    ];

}
