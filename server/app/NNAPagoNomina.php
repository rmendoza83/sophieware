<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAPagoNomina extends Model {

    protected $table = 'nna_pago_nomina';

    protected $fillable = [
        'id_usuario',
        'numero',
        'codigo_clase_nomina',
        'codigo_tipo_nomina',
        'confidencial',
        'fecha_ingreso',
        'fecha_periodo_inicial',
        'fecha_periodo_final',
        'monto_total',
        'observaciones'
    ];

    protected $dates = [
        'fecha_ingreso',
        'fecha_periodo_inicial',
        'fecha_periodo_final'
    ];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'numero' => 'required',
        'codigo_clase_nomina' => 'required',
        'codigo_tipo_nomina' => 'required',
        'confidencial' => 'required',
        'fecha_ingreso' => 'required',
        'fecha_periodo_inicial' => 'required',
        'fecha_periodo_final' => 'required',
        'monto_total' => 'required',
        'observaciones' => 'required'
    ];

}
