<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAClaseNomina extends Model {

    protected $table = 'nna_clase_nomina';

    protected $fillable = [
        'id_usuario',
        'codigo',
        'descripcion',
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo' => 'required',
        'descripcion' => 'required'
    ];

}
