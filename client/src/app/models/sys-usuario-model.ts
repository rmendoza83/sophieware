export class SYSUsuarioModel {
  public id: number;
  public login_usuario: string;
  public nombres: string;
  public apellidos: string;
  public password_usuario: string;
  public email: string;
  public fecha_ingreso: Date;
}