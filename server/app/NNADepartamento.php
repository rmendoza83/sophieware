<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNADepartamento extends Model {

    protected $table = 'nna_departamento';

    protected $fillable = [
        'id_usuario',
        'codigo',
        'nombre',
        'descripcion',
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'codigo' => 'required',
        'nombre' => 'required',
        'descripcion' => 'required'
    ];

}
