<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaDiaNoLaborableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_dia_no_laborable', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->integer('dia');
            $table->integer('mes');
            $table->integer('anno');
            $table->string('descripcion', 40);
            $table->timestamps();
            $table->primary(['id_usuario', 'anno', 'mes', 'dia']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_dia_no_laborable');
    }
}
