<?php

namespace App\Http\Controllers;

use App\SYSUsuario;
use Utils\APIResponseResult;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Database\QueryException;

class SYSUsuarioController extends Controller
{
    public function getlist() {
        return APIResponseResult::OK(SYSUsuario::orderBy('id')->get());
    }

    public function get($id) {
        $model = SYSUsuario::find($id);
        if ($model) {
            return APIResponseResult::OK($model);
        }
        return APIResponseResult::ERROR('Usuario '.$id.' no existe.');
    }

    public function insert(Request $request) {
        $existingModel = SYSUsuario::where('email', $request->email)
            ->orWhere('login_usuario', $request->login_usuario)
            ->first();
        if (!$existingModel) {
            try {
                $model = new SYSUsuario;
                $model->login_usuario = $request->login_usuario;
                $model->nombres = $request->nombres;
                $model->apellidos = $request->apellidos;
                $model->password_usuario = (new BcryptHasher)->make($request->password_usuario);
                $model->email = $request->email;
                $model->fecha_ingreso = $request->fecha_ingreso;
                $model->save();
                return APIResponseResult::OK(); 
            } catch (QueryException $ex) {
                return APIResponseResult::ERROR($ex->getMessage());
            }
        }
        return APIResponseResult::ERROR('Ya existe un Usuario con el login o email especificado.');
    }

    public function update($id, Request $request) {
        $model = SYSUsuario::find($id);
        if ($model) {
            try {
                $model->login_usuario = $request->login_usuario;
                $model->nombres = $request->nombres;
                $model->apellidos = $request->apellidos;
                $model->email = $request->email;
                $model->fecha_ingreso = $request->fecha_ingreso;
                $model->save();
                return APIResponseResult::OK(); 
            } catch (QueryException $ex) {
                return APIResponseResult::ERROR($ex->getMessage());
            }
        }
        return APIResponseResult::ERROR('Usuario '.$id.' no existe. No se puede actualizar el usuario.');
    }

    public function delete($id) {
        $model = SYSUsuario::find($id);
        if ($model) {
            try {
                $model->delete();
                return APIResponseResult::OK(); 
            } catch (QueryException $ex) {
                return APIResponseResult::ERROR($ex->getMessage());
            }
        }
        return APIResponseResult::ERROR('Usuario '.$id.' no existe. No se puede eliminar el usuario.');
    }
}
