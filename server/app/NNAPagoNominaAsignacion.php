<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NNAPagoNominaAsignacion extends Model {

    protected $table = 'nna_pago_nomina_asignacion';

    protected $fillable = [
        'id_usuario',
        'id_nomina_pago_nomina',
        'numero_personal',
        'codigo_concepto',
        'descripcion',
        'cantidad',
        'monto_base',
        'monto_total',
        'observaciones'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
        'id_usuario' => 'required',
        'id_nomina_pago_nomina' => 'required',
        'numero_personal' => 'required',
        'codigo_concepto' => 'required',
        'descripcion' => 'required',
        'cantidad' => 'required',
        'monto_base' => 'required',
        'monto_total' => 'required',
        'observaciones' => 'required'
    ];

}
