export class SESSION_CONSTANTS {
  public static USERNAME = 'Username';
  public static SECURITY_TOKEN = 'SecurityToken';
  public static TOKEN_EXPIRES_IN = 'TokenExpiresIn';
  public static TOKEN_TYPE = 'TokenType';
  public static TOKEN_TIMESTAMP = 'TokenTimestamp';
}