import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { MESSAGE_DIALOG_TYPE } from 'src/app/constants/message-dialog-type';
import { IMessageDialogData } from 'src/app/models/interfaces/message-dialog-data-interface';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent {
  messageDialogType: string = MESSAGE_DIALOG_TYPE.INFORMATION;
  typeMessageDialogType: any = MESSAGE_DIALOG_TYPE;
  title: string = '';
  message: string = '';

  constructor (
    public dialogRef: MatDialogRef<MessageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: IMessageDialogData) {
    this.messageDialogType = dialogData.messageDialogType;
    this.title = dialogData.messageDialogTitle;
    this.message = dialogData.messageDialogMessage;
  }

  onOkClick() {
    //this.dialogRef.close();
    console.log('Ok Click...');
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onYesClick() {
    //this.dialogRef.close
    console.log('Ok Click...');
  }
}
