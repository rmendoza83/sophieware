import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/user/login/login.component';
import { MainMenuComponent } from './components/main/main-menu/main-menu.component';
import { MainComponent } from './components/main/main/main.component';
import { TestPageComponent } from './components/test/test-page/test-page.component';
import { DashboardComponent } from './components/nomina/dashboard/dashboard.component';
import { PagoNominaComponent } from './components/nomina/pago-nomina/pago-nomina.component';
import { TipoNominaComponent } from './components/nomina/tipo-nomina/tipo-nomina.component';
import { ClaseNominaComponent } from './components/nomina/clase-nomina/clase-nomina.component';
import { EmpleadoComponent } from './components/nomina/empleado/empleado.component';
import { DepartamentoComponent } from './components/nomina/departamento/departamento.component';
import { PosicionComponent } from './components/nomina/posicion/posicion.component';
import { ConceptoComponent } from './components/nomina/concepto/concepto.component';
import { FormulaComponent } from './components/nomina/formula/formula.component';
import { VariableCalculoComponent } from './components/nomina/variable-calculo/variable-calculo.component';
import { DiaNoLaborableComponent } from './components/nomina/dia-no-laborable/dia-no-laborable.component';
import { NominaReportComponent } from './components/main/nomina-report/nomina-report.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  {
    path: 'main-menu',
    component: MainMenuComponent,
    children: [
      { path: '', redirectTo: 'test', pathMatch: 'full'},
      { path: 'main', component: MainComponent },
      { path: 'test', component: TestPageComponent },
      { path: 'nna-dashboard', component: DashboardComponent },
      { path: 'nna-pago-nomina', component: PagoNominaComponent },
      { path: 'nna-tipo-nomina', component: TipoNominaComponent },
      { path: 'nna-clase-nomina', component: ClaseNominaComponent },
      { path: 'nna-empleado', component: EmpleadoComponent },
      { path: 'nna-departamento', component: DepartamentoComponent },
      { path: 'nna-posicion', component: PosicionComponent },
      { path: 'nna-concepto', component: ConceptoComponent },
      { path: 'nna-formula', component: FormulaComponent },
      { path: 'nna-variable-calculo', component: VariableCalculoComponent },
      { path: 'nna-dia-no-laborable', component: DiaNoLaborableComponent },
      { path: 'report', component: NominaReportComponent }
    ]
  }
]

/*

      {
        title: 'Configuracion',
        icon: null,
        items: [
          { caption: 'Conceptos de Pago', icon: null, route: 'nna-concepto' },
          { caption: 'Formulas', icon: null, route: 'nna-formula' },
          { caption: 'Variables de Calculo', icon: null, route: 'nna-variable-calculo' },
          { caption: 'Dias No Laborables', icon: null, route: 'nna-dia-no-laborable' }
        ]
      },*/

@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
