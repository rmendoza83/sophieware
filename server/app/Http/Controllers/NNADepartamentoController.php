<?php

namespace App\Http\Controllers;

use App\NNADepartamento;
use Utils\APIResponseResult;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class NNADepartamentoController extends GenericController {

  public function getlist($id_usuario) {
    return APIResponseResult::OK(NNADepartamento::where('id_usuario',$id_usuario)->orderBy('id')->get());
  }

  public function get($id) {
    $model = NNADepartamento::find($id);
    if ($model) {
      return APIResponseResult::OK($model);
    }
    return APIResponseResult::ERROR('Departamento '.$id.' no existe.');
  }

  public function insert(Request $request) {
    $existingModel = NNADepartamento::where('id_usuario', $request->id_usuario)
      ->where('codigo',$request->codigo)
      ->first();
    if (!$existingModel) {
      try {
        $model = new NNADepartamento;
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Ya existe un Departamento con el codigo especificado.');
  }

  public function update($id, Request $request) {
    $model = NNADepartamento::find($id);
    if ($model) {
      try {
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK();
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Departamento '.$id.' no existe. No se puede actualizar el Departamento.');
  }

  public function delete($id) {
    $model = NNADepartamento::find($id);
    if ($model) {
      try {
        $model->delete();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Departamento '.$id.' no existe. No se puede eliminar el Departamento.');
  }
}
