import { DatatypeOptionModel } from './datatype-option-model';

export class FieldDefinitionModel {
    public fieldName: string;
    public caption: string;
    public datatype: string;
    public datatypeOptions: DatatypeOptionModel;
    public htmlClassAttr: string;
    public htmlStyleAttr: Object;
    public showInTable: boolean;

    constructor (
        pFieldName: string,
        pCaption: string,
        pDatatype: string,
        pDatattypeOptions: DatatypeOptionModel,
        pHtmlClassAttr: string,
        pHtmlStyleAttr: string,
        pShowInTable: boolean
    ) {
        this.fieldName = pFieldName;
        this.caption = pCaption;
        this.datatype = pDatatype;
        this.datatypeOptions = pDatattypeOptions;
        this.htmlClassAttr = pHtmlClassAttr;
        this.htmlStyleAttr = this.getStyles(pHtmlStyleAttr);
        this.showInTable = pShowInTable;
    }

    private getStyles(stylesString) {
        var styles = {};

        if (!stylesString) {
            return styles;
        }

        const camelize = function camelize(str) {
            return str.replace(/(?:^|[-])(\w)/g, function (a, c) {
                c = a.substr(0, 1) === '-' ? c.toUpperCase() : c;
                return c ? c : '';
            });
        }
    
        let style = stylesString.split(';');
        for (let i = 0; i < style.length; ++i) {
            const rule = style[i].trim();
    
            if (rule) {
                const ruleParts = rule.split(':');
                const key = camelize(ruleParts[0].trim());
                styles[key] = ruleParts[1].trim();
            }
        }
    
        return styles;
    }
}