<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaFormulaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_formula', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('codigo', 20);
            $table->string('descripcion', 80);
            $table->decimal('factor_multiplicador', 18, 4);
            $table->text('texto_formula');
            $table->timestamps();
            $table->primary(['id_usuario', 'codigo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_formula');
    }
}
