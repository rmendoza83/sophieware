<?php

namespace App\Http\Controllers;

use App\NNAPosicion;
use Utils\APIResponseResult;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class NNAPosicionController extends GenericController {

  public function getlist($id_usuario) {
    return APIResponseResult::OK(NNAPosicion::where('id_usuario',$id_usuario)->orderBy('id')->get());
  }

  public function get($id) {
    $model = NNAPosicion::find($id);
    if ($model) {
      return APIResponseResult::OK($model);
    }
    return APIResponseResult::ERROR('Posicion '.$id.' no existe.');
  }

  public function insert(Request $request) {
    $existingModel = NNAPosicion::where('id_usuario', $request->id_usuario)
      ->where('codigo',$request->codigo)
      ->first();
    if (!$existingModel) {
      try {
        $model = new NNAPosicion;
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Ya existe una Posicion con el codigo especificado.');
  }

  public function update($id, Request $request) {
    $model = NNAPosicion::find($id);
    if ($model) {
      try {
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK();
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Posicion '.$id.' no existe. No se puede actualizar la Posicion.');
  }

  public function delete($id) {
    $model = NNAPosicion::find($id);
    if ($model) {
      try {
        $model->delete();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Posicion '.$id.' no existe. No se puede eliminar la Posicion.');
  }
}
