<?php

namespace App\Http\Controllers;

use App\GenTipoIdentificacion;
use Utils\APIResponseResult;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class GenTipoIdentificacionController extends GenericController {

  public function getlist() {
    return APIResponseResult::OK(GenTipoIdentificacion::orderBy('id')->get());
  }

  public function get($id) {
    $model = GenTipoIdentificacion::find($id);
    if ($model) {
      return APIResponseResult::OK($model);
    }
    return APIResponseResult::ERROR('Tipo de Identificacion '.$id.' no existe.');
  }

  public function insert(Request $request) {
    $existingModel = GenTipoIdentificacion::where('codigo', $request->codigo)
      ->first();
    if (!$existingModel) {
      try {
        $model = new GenTipoIdentificacion;
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Ya existe un Tipo de Identificacion con el codigo especificado.');
  }

  public function update($id, Request $request) {
    $model = GenTipoIdentificacion::find($id);
    if ($model) {
      try {
        $this->fillModel($model, $request);
        $model->save();
        return APIResponseResult::OK();
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Tipo de Identificacion '.$id.' no existe. No se puede actualizar el Tipo de Identificacion.');
  }

  public function delete($id) {
    $model = GenTipoIdentificacion::find($id);
    if ($model) {
      try {
        $model->delete();
        return APIResponseResult::OK(); 
      } catch (QueryException $ex) {
        return APIResponseResult::ERROR($ex->getMessage());
      }
    }
    return APIResponseResult::ERROR('Tipo de Identificacion '.$id.' no existe. No se puede eliminar el Tipo de Identificacion.');
  }
}
