export interface IDialogResult {
  submitted: boolean;
  data: any;
}