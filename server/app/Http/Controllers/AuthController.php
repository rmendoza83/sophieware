<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;

use Utils\APIResponseResult;

class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password_usuario' => 'required',
        ]);
        try 
        {
            $credentials = [
                'email' => $request['email'],
                'password' => $request['password_usuario']
            ];
            if (! $token = $this->jwt->attempt($credentials))
            {
                return APIResponseResult::ERROR('User not found or invalid credentials');
            }
        } 
        catch (TokenExpiredException $e)
        {
            return APIResponseResult::ERROR('Token Expired :: ' . $e->getMessage());
        }
        catch (TokenInvalidException $e)
        {
            return APIResponseResult::ERROR('Token Invalid :: ' . $e->getMessage());
        }
        catch (JWTException $e) 
        {
            return APIResponseResult::ERROR('Token Absent :: ' . $e->getMessage());
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function current_user()
    {
        try
        {
            $current_user = $this->jwt->user();
            if ($current_user)
            {
                return APIResponseResult::OK($current_user);
            }
            return APIResponseResult::ERROR('Invalid Session');
        }
        catch (TokenExpiredException $e)
        {
            return APIResponseResult::ERROR('Token Expired :: ' . $e->getMessage());
        }
        catch (TokenInvalidException $e)
        {
            return APIResponseResult::ERROR('Token Invalid :: ' . $e->getMessage());
        }
        catch (JWTException $e) 
        {
            return APIResponseResult::ERROR('Token Absent :: ' . $e->getMessage());
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try
        {
            $token = $this->jwt->getToken();
            if ($token)
            {
                $this->jwt->invalidate($token);
                return APIResponseResult::OK([
                    'message' => 'Successfully logged out'
                ]);
            }
            return APIResponseResult::ERROR('Invalid Session');
        }
        catch (TokenExpiredException $e)
        {
            return APIResponseResult::ERROR('Token Expired :: ' . $e->getMessage());
        }
        catch (TokenInvalidException $e)
        {
            return APIResponseResult::ERROR('Token Invalid :: ' . $e->getMessage());
        }
        catch (JWTException $e) 
        {
            return APIResponseResult::ERROR('Token Absent :: ' . $e->getMessage());
        }
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try
        {
            $token = $this->jwt->getToken();
            if ($token)
            {
                return $this->respondWithToken($this->jwt->refresh());
            }
            return APIResponseResult::ERROR('Invalid Session');
        }
        catch (TokenExpiredException $e)
        {
            return APIResponseResult::ERROR('Token Expired :: ' . $e->getMessage());
        }
        catch (TokenInvalidException $e)
        {
            return APIResponseResult::ERROR('Token Invalid :: ' . $e->getMessage());
        }
        catch (JWTException $e) 
        {
            return APIResponseResult::ERROR('Token Absent :: ' . $e->getMessage());
        }
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */ 
    protected function respondWithToken($token)
    {
        return APIResponseResult::OK([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->jwt->factory()->getTTL() * 60
        ]);
    }
}
