<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNnaPagoNominaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nna_pago_nomina', function (Blueprint $table) {
            $table->increments('id');
            $table->dropPrimary();
            $table->integer('id_usuario');
            $table->string('numero', 20);
            $table->string('codigo_clase_nomina', 10);
            $table->string('codigo_tipo_nomina', 10);
            $table->boolean('confidencial');
            $table->date('fecha_ingreso');
            $table->date('fecha_periodo_inicial');
            $table->date('fecha_periodo_final');
            $table->decimal('monto_total', 18, 4);
            $table->text('observaciones');
            $table->timestamps();
            $table->primary(['id_usuario', 'numero']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nna_pago_nomina');
    }
}
